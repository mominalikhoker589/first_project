from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from rest_framework import routers
from custom_auth.views import MyObtainTokenPairView, RegisterView, HelloView
from rest_framework_simplejwt.views import TokenRefreshView
from django.conf.urls import include


router = routers.DefaultRouter()
# router.register(r'custom_name', MyObtainTokenPairView, basename='custom_name')

urlpatterns = [
    path('login/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name='auth_register'),
    path('login/refresh/hello/', HelloView.as_view()),
    path(r'^', include(router.urls)),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
