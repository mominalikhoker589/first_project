from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    TaskListApiView, VehicleApiView, VehicleViewSet,
)

router = DefaultRouter()
router.register('vehicle', VehicleViewSet, basename="vehicle")


urlpatterns = [
    path('', TaskListApiView.as_view()),
    path('filtering/', VehicleApiView.as_view()),
    # path('/api/vieset', ItemModelViewSet.as_view),
    path("", include(router.urls)),

]
