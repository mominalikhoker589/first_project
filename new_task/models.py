from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Task(models.Model):
    task = models.CharField(max_length=180)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, blank=False, null=False)
    completed = models.BooleanField(default=False, blank=False, null=False)
    updated = models.DateTimeField(auto_now=True, blank=False, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)


class Task2(models.Model):
    name = models.CharField(max_length=30)
    toppings = models.ManyToManyField('Topping')

    def __str__(self):
        return self.name


class Topping(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Vehicle(models.Model):
    reg_no = models.IntegerField()
    owner = models.CharField(max_length=100, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)


class Car(models.Model):
    vehicle = models.OneToOneField(Vehicle,
                                   on_delete=models.CASCADE, primary_key=True)
    car_model = models.CharField(max_length=100)
