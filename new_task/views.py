from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from rest_framework_simplejwt.authentication import JWTAuthentication

from .models import Task, Task2, Vehicle
from .Serializer import TaskSerializer, Task2Serializer, VehicleSerializer


# Create your views here.


class TaskListApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        tasks = Task.objects.filter(user=request.user.id)
        serializer = TaskSerializer(tasks, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            'task': request.data.get('task'),
            'completed': request.data.get('completed'),
            'user': request.user.id
        }
        serializer = TaskSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Task2ApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = Task2Serializer
    queryset = Task2.objects.all()


class VehicleApiView(APIView):            # <-- And here Authentication
    authentication_classes = [JWTAuthentication]
    serializer_class = VehicleSerializer
    queryset = Vehicle.objects.all()
    # filterset_fields = ('owner',)

    def get_queryset(self):
        qs = super().get_queryset()
        only_missing = str(self.request.query_params.get('missing')).lower()
        if only_missing in ['true', '1']:
            return qs.filter(returned__isnull=True)
        return qs

    # def get_or_create(self, request):
    #     pass

    # your current definition here

    def get(self, request):
        return Response({'Tbhai': 'you just hit the get request'}, status=200)

    def post(self, request):
        return Response({'Tbhai': 'you just hit the post request'}, status=200)


class VehicleViewSet(ModelViewSet):
    authentication_classes = [JWTAuthentication]
    serializer_class = VehicleSerializer
    queryset = Vehicle.objects.all()

    def list(self, request, *args, **kwargs):
        self.queryset = Vehicle.objects.filter(user=request.user.id)
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    # create
    def create(self, request, *args, **kwargs):
        request.data._mutable = True
        request.data['user'] = request.user.id
        return super(VehicleViewSet, self).create(request, *args, **kwargs)

