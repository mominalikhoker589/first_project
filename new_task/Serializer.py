from rest_framework import serializers
from .models import Task, Task2, Vehicle


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'


class Task2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Task2
        fields = '__all__'


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'
